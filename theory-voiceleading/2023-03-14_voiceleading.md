# Notes on voice leading

## Context: "Common practice era"
Harmony ca. 1700-1900 (give or take a few years)

### rules or guidelines?
 * handled as hard and fast rules in educational contexts
 * can be handled more liberally in actual composing, kind of "best practice" guidelines
 * historic development: dissonance was handled increasingly liberally throughout the period called "Common Practice era"

### 4 part choir:
 1. 4 voices, no divisions (think soloist SATB ensemble)
 2. ambitus of lay choir: usually 1 1/2 octaves per voice

### best practices aka THE RULES
 1. avoid parallel octaves and fifths (this one's important!)
 2. prefer contrary motion between voices (one voice staying the same, the other moving is also good)
 3. avoid crossing between voices (e.g. alto above soprano)
 4. stepwise motion within a voice is preferred
 5. keep common notes
 6. take the shortest way between chords
