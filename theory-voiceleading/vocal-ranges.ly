\version "2.24.1"
\language "deutsch"

\header {
  title = "vocal ranges"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key c \major
  \time 4/4
}

sopranoVoice = \relative c' {
  \global
  \dynamicUp
  c2 g''2
  % Alto
  g,, c'
  % Tenor
  \clef bass
  c,, g''
  % Bass 
  f,, c''
}

verse = \lyricmode {
  % Liedtext folgt hier.
  
}

\score {
  \new Staff \with {
    instrumentName = "S."
    midiInstrument = "choir aahs"
  } { \sopranoVoice }
  \addlyrics { \verse }
  \layout { }
  \midi {
    \tempo 4=100
  }
}
