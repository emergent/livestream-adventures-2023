// 2023-06-27
"setup.scd".loadRelative;
// soundcheck
{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src);
{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src, outbus:~bus[\reverb]);


// patterns! ya!
// arp pattern
ControlPoster.pbind(\fbsinvoice);
(
~fbsinvoicePat = Pmono(
	\fbsinvoice,
	\dur, 1/(Pdup(Plprand(4, 8), Pmeanrand(2, 4))),
	\scale, Scale.locrian(\just),
	\octave, Prand((4..6), inf),
	\degree, Pwalk([0, 2, 4, 6], Prand([2, 1, -1, -2], inf), Prand([-1, 1], inf)),
	\dry, 0.5,
	\atk, 0.1,
	\dec, 0.2,
	\suslev, 1.0,
	\rel, 2.0,
	\feedback, Pwhite(1.5, 2),
	\amp, -12.dbamp,
	\pan, Pseq([-1, -0.5, 0, 0.5, 1].mirror, 16),
	\group, ~src,
	\fx, ~bus[\reverb],
	\out, ~bus[\endstage]
);
)

c = ~fbsinvoicePat.play(t);
// kthxbai!
s.quit;
