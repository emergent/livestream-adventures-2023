# Livestream Adventures 2023

SuperCollider files that I write during my weekly livestreams on [my twitch channel](https://twitch.tv/the_emergent).
These files are intended for information, education and entertainment. I do my best to check if the code is working as intended before publication; however, I cannot give any guarantees that the information they contain is complete and accurate, nor can I guarantee that they will produce the intended results on your machine.
Use this code at your own risk.

This code is licensed under the [Peer Production License](https://civicwise.org/peer-production-licence-_-human-readable-summary/). Feel free to do things with it as long as you credit me, share your creation under the same license, and don't use it in capitalist exploitation.
