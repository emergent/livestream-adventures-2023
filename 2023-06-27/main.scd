// 2023-06-27
"setup.scd".loadRelative;
// soundcheck
{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src);
{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src, outbus:~bus[\reverb]);
s.quit;
