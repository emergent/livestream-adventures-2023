// synthdefs for techno rumble
// bass drum
(
SynthDef(\bd, {
	arg dry = 1;
	var sig, env;

	env = Env.adsr(\atk.ir(0.001), \dec.ir(0.2), \suslev.ir(1), \rel.ir(1)).kr(2, \gate.kr(1));
	
	sig = AnalogBassDrum.ar(
		Impulse.kr(\beat.kr(3)), // TODO: make it so this conforms to bpm 
		0,
		\accent.kr(0.05),
		\freq.kr(35),
		\tone.kr(0.4),
		\decay.kr(1.2),
		\attackfm.kr(0.4),
		\selffm.kr(0.75)
	);

	sig = BLowShelf.ar(
			sig,
			\cf.kr(140),
			\rs.kr(0.5),
			\db.kr(9) // use db values here!
		);

	sig = Compander.ar(
		sig,
		sig,
		\thresh.kr(0.5),
		\slopeBelow.kr(1),
		\slopeAbove.kr(0.25),
		\clamp.kr(0.01),
		\relax.kr(0.25)
	);

	sig = sig * env * \amp.kr(-3.dbamp);

	Out.ar(\out.kr(0), Pan2.ar(sig * dry, \pan.kr(0)));
	Out.ar(\fx.kr(0), sig * (1- dry));
}).add;

// effects: rumble
SynthDef(\rumble, {
	var in, sig;
	
	in = In.ar(\in.kr(0), 1);

	// delay
	sig = AllpassL.ar(
		in,
		\maxdel.kr(1),
		\dtime.kr(0.2),
		\dectime.kr(1)
	);

	// reverb
	sig = FreeVerb.ar(
		sig,
		0.75, 
		\room.kr(0.8),
		\damp.kr(0.33)
	);

	// saturation
	sig = NonlinearFilter.ar(
		sig, 
		\filterfreq.kr(500),
		\filterq.kr(0.25),
		\filtergain.kr(1),
		\filtershape.kr(5),
		\saturate.kr(1)
	);

	// lowpass
	sig = BLowPass.ar(
		sig,
		\cutoff.kr(600),
		\rq.kr(1)
	);

	// eq
	sig = BPeakEQ.ar(
		sig,
		\eqfreq.kr(600),
		\eqrq.kr(1),
		\eqdb.kr(-12)
	);

	// sidechain compression
	sig = Compander.ar(
		sig,
		in,
		\thresh.kr(0.1),
		\slopeBelow.kr(1),
		\slopeAbove.kr(0.125),
		\clamp.kr(0.001),
		\relax.kr(0.25)
	);

	sig = sig * \amp.kr(-3.dbamp);

	sig = Pan2.ar(sig, 0);

	Out.ar(\out.ir(0), sig);
}).add;

// lvl3
/* 
 * duplicate the rumble signal, but don't lowpass it, instead bandpass the mids 
*/

// effects: "endstage"
// * add saturation, eq (control low mid), gentle limiter (1-2db) to the mix of dry+rumble signal
SynthDef(\endstage, {
	var sig;
	
	sig = In.ar(\in.kr(0), 2); // route both dry drum as well as output from rumble synth here
	// here go: saturation, eq (low mid), limiter (1-2 db)

	// saturation
	sig = sig.blend(sig.tanh, 0.75);
	
	// eq
	sig = BPeakEQ.ar(
		sig,
		\eqfreq.kr(800),
		\eqrq.kr(0.75),
		\eqdb.kr(-6)
	);

	// limiter
	sig = Compander.ar(
		sig,
		sig,
		\thresh.kr(0.8),
		\slopeBelow.kr(1),
		\slopeAbove.kr(0.1),
		\clamp.kr(0.01),
		\relax.kr(0.01)
	);

	sig = sig * \amp.kr(-6.dbamp);

	sig = Pan2.ar(sig, 0);

	Out.ar(\out.kr(0), sig);
}).add;
)


