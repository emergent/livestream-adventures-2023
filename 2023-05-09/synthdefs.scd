
// Fun with DWGBowed
(
SynthDef(\bowed, {
	arg dry = 1;
	var sig, env, vib, reso, freq, at;
	freq = \freq.kr(220, 0.02).clip(20,20000);
	at = \atk.ir(0.25) * freq.linexp(20, 20000, 2, 0.5);
	env = Env.asr(at, 1, \rel.kr(0.8)).kr(2, \gate.kr(1));
	vib = SinOsc.kr(
		LFNoise2.kr(0.2).range(4,6),
		mul: LFNoise2.kr(0.05).bipolar(2)
	);

	sig = DWGBowed.ar(
		freq + vib,
		\velb.kr(0.5),
		\force.kr(0.8),
		pos: \pos.kr(0.14),
		release: \release.kr(0.1),
		c1: \c1.kr(0.25),
		c3: \c3.kr(30),
		impZ: \imped.kr(0.55),
		fB: \inharm.kr(2)
	) * 0.2;

	sig = DWGSoundBoard.ar(sig);

	reso = BPF.ar(sig, 
		\resFreqs.kr([275, 405, 460, 530, 700], 0.05), 
		\rqs.kr([1, 0.8, 0.75, 0.9, 0.1], 0.05), 
		\filterAmps.kr([0.99, 0.5, 0.9, 0.25, 0.15], 0.05)
	).sum;

	sig = sig + reso;
	sig = sig * 0.2;

	sig = LPF.ar(sig, \lpCutoff.kr(6000, 0.05));

	sig = sig * env * \amp.kr(-6.dbamp, \amplag.kr(0.08));

	sig = Pan2.ar(sig, \pan.kr(0, 0.05));

	Out.ar(\out.kr(0), sig * dry);
	Out.ar(\fx.kr(0), sig * (1-dry));
}).add;

SynthDef(\square, {
	arg dry = 0.5;
	var sig1, sig2, sig, env, freq, dt, at;

	freq = \freq.kr(220, 0.02).clip(20,20000);
	at = \atk.ir(0.1) * freq.linexp(20, 20000, 2, 0.5);
	dt = LFNoise2.kr(\dtfreq.kr(0.2!5)).bipolar(\dtune.kr(0.15, 0.05)).midiratio;
	freq = freq * dt;
	
	env = Env.adsr(at, \dec.ir(0.2), \suslev.ir(1), \rel.ir(2), curve: \curve.kr(-2)).kr(2, \gate.kr(1));

	//square
	sig1 = PulseDPW.ar(
		freq,
		\pwidth.kr(0.5),
		mul: 0.1
	).sum;

	// saw
	sig2 = VarSaw.ar(
		freq,
		0,
		\swidth.kr(0.0001),
		mul: 0.1
	).sum;
	sig2 = LPF.ar(sig2, \cutoff.kr(3200, 0.075).clip(50, 20000));

	sig = sig1.blend(sig2, \blend.kr(0.5, 0.05));
	sig = sig * env * \amp.kr(-6.dbamp);

	sig = Pan2.ar(sig, \pan.kr(0));

	Out.ar(\out.ir(0), sig * dry);
	//Out.ar(\sendbus.ir(0), sig * \send.kr(0.25));
	Out.ar(\fx.ir(0), sig * (1- dry));
}).add;
)
