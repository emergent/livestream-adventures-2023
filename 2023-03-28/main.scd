// More strategies for building compositions
"setup.scd".loadRelative;
"synthdefs.scd".loadRelative;

// soundcheck
{PinkNoise.ar(0.2!2)*Env.perc.kr(2)}.play(target:~src);
{PinkNoise.ar(0.2!2)*Env.perc.kr(2)}.play(target:~src, outbus: ~bus[\reverb]);

~revSynth.set(\revAmp, 0.dbamp);

(
~base = Pbind(
	\scale, Scale.aeolian(\just),
	\root, 7, 
	\octave, 4,
	\dur, 1,
	\out, ~out,
	\fx, ~bus[\reverb],
	\group, ~src
);

~bow2 = Pbind(
	\instrument, \bowed,
	\dur, 0.5,
	\dry, 0.6,
	\atk, 0.25,
	\rel, 0.8,
	\velb, Pmeanrand(0.25, 0.75),
	\force, Phprand(0.5, 0.9),
	\pos, Plprand(0.04, 0.08),
	\c1, Pwhite(0.25, 0.3),
	\c3, 60,
	\degree, Pseq([
		Pwalk([0, 3, 4, 6], Pseq([1], 8), Pseq([-1, 1], 8), 3),
		Pwalk([1, 3, 5, 7], Pseq([1], 8), Pseq([-1, 1], 8), 2),
		Pwalk([0, 2, 5, 6], Pseq([1], 8), Pseq([1, -1], 8), 1),
		Pwalk([-1, 1, 4, 6], Pseq([1], 8), Pseq([1, -1], 8), 0)
	], 4),
	\amp, Pseq([-2, -3, -4.5, Pn(Pwhite(-6, -3), 5)].dbamp, 24) + 1.5.dbamp,
	\legato, Pseq([Phprand(0.98, 1.1, 2), 0.5, Plprand(0.07, 0.5, 5)], inf)
) <> ~base;

~bow1 = Pbind(
	\instrument, \bowed,
	\dry, 0.6,
	\atk, 0.25,
	\rel, 0.8,
	\velb, Pmeanrand(0.25, 0.75),
	\force, Phprand(0.5, 0.9),
	\pos, Plprand(0.04, 0.08),
	\c1, Pwhite(0.25, 0.3),
	\c3, 60,
	\dur, 0.25,
	\degree, Pseq((0..7), 8),
	\amp, Pn(Pseries(-1.5, -1, 8).dbamp, 8) + 1.5.dbamp,
	\legato, 0.33
) <> ~base;
)

a = ~bow2.play(t, quant:1);
a.stop;

b = ~bow1.play(t, quant:1);
b.stop;

d = Synth(\bowed, [\out, ~out, \fx, ~bus[\reverb], \dry, 0.6, \freq, 36.midicps], ~src);
d.set(\gate, 0);

// added a bass synthdef - now let's develop a bass pattern!
// exploring a the synthdefs sounds to find a good bass sound
(
Pbindef(\square,
	\instrument, \square,
	\dur, 1,
	\scale, Scale.minorPentatonic,
	\octave, Prand((2..6), inf),
	\degree, Prand((0..5), inf),
	\dry, 0.5,
	\dtfreq, 0.4,  
	\dtune, 0.21,
	\atk, 0.01,
	\dec, 0.5,
	\suslev, 0.3,
	\rel, 1.5,
	\curve, -8.0,
	\pwidth, 0.5,
	\swidth, 0.01,
	\cutoff, 660,
	\blend, 0.1,
	\amp, 0.5,
	\legato, 0.5,
//	\sendbus, 0.0,
//	\send, 0.25,
	\group, ~src,
	\fx, ~bus[\reverb]
);
)

Pbindef(\square).play(t);
Pbindef(\square).fadeTime_(2);
Pbindef(\square).stop;

(
~bass = Pbind(
	\instrument, \square,
	\octave, 3,
	\dur, 2,
	\degree, Pseq(
		[
			Pseq([0, 4]),
			Pseq([1, 5]),
			Pseq([2, 5]),
			Pseq([-1, 4])
		],
	4),
	\dry, 0.6,
	\amp, Pseq([-3, Pwhite(-6, -4.5, 3)], inf).dbamp,
	\dtfreq, 0.4,  
	\dtune, 0.21,
	\atk, 0.1,
	\dec, 0.75,
	\suslev, 0.3,
	\rel, 1.5,
	\curve, -6.0,
	\pwidth, 0.5,
	\swidth, 0.01,
	\cutoff, 660,
	\blend, 0.1,
	\legato, 0.5
) <> ~base;
)

e = ~bass.play(t, quant:1);
e.stop;

(
f = Ppar([
	~bow2,
	~bass 
]).play(t, quant:1);
)
f.stop;

// put the patterns in an event dictionary
(
~ev = Dictionary.new;
~ev.add(\scales -> {~bow1.play(t, quant:t.beatsPerBar)});
~ev.add(\arp -> {~bow2.play(t, quant:t.beatsPerBar); ~bass.play(t, quant:t.beatsPerBar)});
);

~bow1.reset;
~ev[\scales].value
~ev[\arp].value;

// next time: 
// * triggering events with midi
// * try out ways of storing the events in a function that gets executed on ServerBoot/ServerTree
// * data sharing with embedded streams
// * other ways of data sharing? (Plambda, Pset, Pget)

//kthxbye!
s.quit;
