---
title: Theory session – Dissonance
documentclass: fancyhandout
classoptions: 
 - 12pt
 - rmfont
---
# Dissonance

## What is dissonance anyway?
Physical hypotheses:
 - simple ratios between fundamental frequencies appear consonant, complex interval ratios are heard as dissonant
 - beating between overtones

## Music history: The evolution of the treatment of dissonance
Over the course of the so called Common Practice era, the treatment of dissonance saw some development. 

## Handling dissonance in a common practice context
How dissonance is handled, and what forms are permitted, depends on where in the measure it occurs.

Important concept: **resolution** of a dissonance into a consonant interval; the dissonance is heard as "striving towards" a consonant interval.

### Dissonance on the un-emphasized beat

* **Passing note**
* **Cambiata**
* **Anticipation**
* **Ornamental notes** (appogiatura, grace note)

### Dissonance on the emphasized beat
Passing notes can sometimes occur on the downbeat
Mainly: suspension

 * 4-3
 * 9-8

Originally, the "suspended" note had to be tied over from the chord before, although that rule disappeared gradually. 
Suspended notes are usually resolved stepwise down.

Unlike in pop/jazz, suspensions aren't treated as independent chords in classical music.

