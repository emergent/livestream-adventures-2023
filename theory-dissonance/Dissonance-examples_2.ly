\version "2.24.1"
\language "deutsch"

\header {
  title = "Dissonance examples"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key c \major
  \time 4/4
}

rightOne = \relative c'' {
  \global
  % Die Noten folgen hier.
  f,1^"D7 and its inversions" g h d \bar "||" \break
  f,2^"D7 and its resolutions" e 
  g g
  h c
  d c \bar "||" \break
  <f, a>1^"Major ninth chord" <f as>^"Minor ninth chord"
  <f a>2 <e g>
  \bar "||" \break
  as1^"Diminished 7th chord"
  cis^"Augmented chord" \bar "||" \break
  c2^"cadence with sixte ajout́ée" d d c
}

rightTwo = \relative c'' {
  \global
  % Die Noten folgen hier.
  d,1 f g h
  d,2 c
  f e
  g g
  h c
  d,1 d
  d2 c
  f1
  a
  <g e>2 <c a> <h g> <g e>
}

leftOne = \relative c' {
  \global
  % Die Noten folgen hier.
  h1 d 
  \clef treble
  f g
  \clef bass
  h,2 g
  d' c
  \clef treble
  f e
  g g
  \clef bass
  h,1 h
  h2 g
  d'1
  \clef treble
  f
  \clef bass
  r1 r
}

leftTwo = \relative c' {
  \global
  % Die Noten folgen hier.
  g1 h 
  \clef treble 
  d f
  \clef bass
  g,2 c,
  h' c
  \clef treble 
  d c
  f e
  \clef bass
  g,1 g
  g2 c,
  h'1
  \clef treble
  f'
  \clef bass
  c,2 f g c, 
}

\score {
  \new PianoStaff \with {
    instrumentName = "Piano"
  } <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } << \rightOne \\ \rightTwo >>
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass << \leftOne \\ \leftTwo >> }
  >>
  \layout {
    line-width = #150
    ragged-right = ##t
  }
  \midi {
    \tempo 4=80
  }
}
