---
title: Theory session – Dissonance
documentclass: fancyhandout
classoptions: 
 - 12pt
 - rmfont
---
# Dissonant chords 

* D7 and its inversions: The seventh has to be resolved stepwise down, the third of the dominant chord has to go up a half step.
* D9
* diminished/diminished 7th: conceived of as "abbreviated" version of the D7/D79, but highly re-interpretable and thus an excellent chord for fast modulation
* augmented chord
* sixte ajoutée
* chords with chromatic alteration: mostly lowered 5th of a D7 or D79
