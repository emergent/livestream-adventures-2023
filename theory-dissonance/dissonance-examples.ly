\version "2.24.1"
\language "deutsch"

\header {
  title = "Dissonance examples"
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key c \major
  \time 4/4
}

soprano = \relative c'' {
  \global
  % Die Noten folgen hier.
  % Intervals from most consonant to most dissonant
  c1 g f e
  es a as b 
  d, h' des, fis \bar "||" \break
  c'4^"passing note" h a2 \bar "||"
  c4^"cambiata" h c2  \bar "||" %can also go up
  h2.^"anticipation" c4 c1 \bar "||" \break
  
  % dissonance on the emphasized beat: suspension
  d1^"suspended forth" e \bar "||" 
  d2^"suspended ninth" c \bar "||" 
}

alto = \relative c' {
  \global
  % Die Noten folgen hier.
  c1 c c c 
  c c c c 
  c c c c
  g'2 e
  e1
  g1 g
  c2 h c1
  g1
}

tenor = \relative c' {
  \global
  % Die Noten folgen hier.
  r1 r r r
  r r r r
  r r r r
  e2 c
  g1
  d' e
  \clef treble
  g g
  \clef bass
  e 
}

bass = \relative c {
  \global
  % Die Noten folgen hier.
  r1 r r r
  r r r r
  r r r r
  c2 a'2 |
  c,1
  g' c
 \clef treble
  g c
  \clef bass
  c,
}
% 
% verse = \lyricmode {
%   % Liedtext folgt hier.
%   
% }

% rehearsalMidi = #
% (define-music-function
%  (parser location name midiInstrument lyrics) (string? string? ly:music?)
%  #{
%    \unfoldRepeats <<
%      \new Staff = "soprano" \new Voice = "soprano" { \soprano }
%      \new Staff = "alto" \new Voice = "alto" { \alto }
%      \new Staff = "tenor" \new Voice = "tenor" { \tenor }
%      \new Staff = "bass" \new Voice = "bass" { \bass }
%      \context Staff = $name {
%        \set Score.midiMinimumVolume = #0.5
%        \set Score.midiMaximumVolume = #0.5
%        \set Score.tempoWholesPerMinute = #(ly:make-moment 100 4)
%        \set Staff.midiMinimumVolume = #0.8
%        \set Staff.midiMaximumVolume = #1.0
%        \set Staff.midiInstrument = $midiInstrument
%      }
%      \new Lyrics \with {
%        alignBelowContext = $name
%      } \lyricsto $name $lyrics
%    >>
%  #})

\score {
  \new ChoirStaff <<
    \new Staff \with {
      midiInstrument = "choir aahs"
      instrumentName = \markup \center-column { "S." "A." }
    } <<
      \new Voice = "soprano" { \voiceOne \soprano }
      \new Voice = "alto" { \voiceTwo \alto }
    >>
  %   \new Lyrics \with {
%       \override VerticalAxisGroup #'staff-affinity = #CENTER
%     } \lyricsto "soprano" \verse
    \new Staff \with {
      midiInstrument = "choir aahs"
      instrumentName = \markup \center-column { "T." "B." }
    } <<
      \clef bass
      \new Voice = "tenor" { \voiceOne \tenor }
      \new Voice = "bass" { \voiceTwo \bass }
    >>
  >>
  \layout {
    line-width = #150
    ragged-right = ##t
  }
  % \midi {
%     \tempo 4=100
%   }
}

% MIDI-Dateien zum Proben:
% \book {
%   \bookOutputSuffix "soprano"
%   \score {
%     \rehearsalMidi "soprano" "soprano sax" \verse
%     \midi { }
%   }
% }
% 
% \book {
%   \bookOutputSuffix "alto"
%   \score {
%     \rehearsalMidi "alto" "soprano sax" \verse
%     \midi { }
%   }
% }
% 
% \book {
%   \bookOutputSuffix "tenor"
%   \score {
%     \rehearsalMidi "tenor" "tenor sax" \verse
%     \midi { }
%   }
% }
% 
% \book {
%   \bookOutputSuffix "bass"
%   \score {
%     \rehearsalMidi "bass" "tenor sax" \verse
%     \midi { }
%   }
% }

